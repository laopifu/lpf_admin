<?php

use think\migration\Migrator;

class File extends Migrator {
    /**
     * 上传文件保存数据 默认InnoDB 表名自带表前缀 自动创建id字段，
     */
    public function change() {
        $table = $this->table('file');
        $table->addColumn('file_name', 'string', array('limit' => 200, 'default' => '', 'comment' => '文件名'))
            ->addColumn('file_path', 'string', array('limit' => 300, 'default' => '', 'comment' => '文件路径'))
            ->addIndex(array('id'), array('unique' => true))
            ->setComment('上传文件保存')
            ->create();
    }
}
