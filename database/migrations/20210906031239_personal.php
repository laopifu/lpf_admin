<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Personal extends Migrator
{
    /**
     * 微信用户表 默认InnoDB 表名自带表前缀 自动创建id字段，
     */
    public function change() {
        $table = $this->table('personal');
        $table->addColumn('openid', 'string', array('limit' => 50, 'default' => 0, 'comment' => 'openid'))
            ->addColumn('unionid', 'string', array('limit' => 50, 'default' => '', 'comment' => '公众平台身份id'))
            ->addColumn('sex', 'boolean', array('limit' => 1, 'default' =>1, 'comment' => '性别'))
            ->addColumn('nickname', 'string', array('limit' => 50, 'default' => '', 'comment' => '用户名'))
            ->addColumn('avatar', 'string', array('limit' => 50, 'default' => '', 'comment' => '头像'))
            ->addColumn('province', 'string', array('limit' => 10, 'default' => '', 'comment' => '省'))
            ->addColumn('city', 'string', array('limit' => 10, 'default' => '', 'comment' => '城市'))
            ->addColumn('create_time', 'string', array('limit' => 10, 'default' => '', 'comment' => '注册时间'))
            ->addColumn('login_time', 'string', array('limit' => 10, 'default' => '', 'comment' => '登陆时间'))
            ->addColumn('status', 'boolean', array('limit' => 1, 'default' => 1, 'comment' => '1正常0禁用'))
            ->addIndex(array('id'), array('unique' => true))
            ->setComment('微信用户表')
            ->create();
        /*nickname修改为utf8mb4格式*/
        \think\facade\Db::query("ALTER TABLE `lpf_personal`
MODIFY COLUMN `nickname`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户名' AFTER `sex`;");
    }
}
