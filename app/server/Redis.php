<?php
/**
 * redis
 */

namespace app\server;

use think\facade\Cache;

class Redis {
    /**
     * @var
     */
    protected $Cache;

    public function __construct() {
        $this->Cache = Cache::store('redis');
    }

    /**
     * 后台用户token
     * @param int $key
     * @param string $value
     */
    public function setToken(int $key, string $value) {
        $this->Cache->set('access_token:' . $key, $value, 86400);
    }

    /**
     * @param int $key
     * @return mixed
     */
    public function getToken(int $key) {
        return $this->Cache->get('access_token:' . $key);
    }

    /**
     * @param int $key
     * @return mixed
     */
    public function delToken(int $key) {
        return $this->Cache->del('access_token:' . $key);
    }

    public function setAuthRule(string $value){
        $this->Cache->set('auth_rule', $value);
    }
    public function getAuthRule(){
        return $this->Cache->get('auth_rule');
    }

    /**
     * 微信用户token
     * @param string $key
     * @param string $value
     */
    public function setWechatToken(string $key, string $value) {
        $this->Cache->set('wx_token:' . $key, $value, 86400*7);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getWechatToken(string $key) {
        return $this->Cache->get('wx_token:' . $key);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function delWechatToken(string $key) {
        return $this->Cache->del('wx_token:' . $key);
    }
}
