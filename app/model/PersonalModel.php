<?php

namespace app\model;

use think\Model;

class PersonalModel extends Model {
    protected $pk = 'id';

    protected $name = 'personal';
    // 设置字段信息
    protected $type = [
        'login_time' => 'timestamp',
        'create_time' => 'timestamp',
    ];

    public static function getList(array $where, int $page, int $limit, string $field = '',string $outfield = '') {
        $list = self::where($where)->page($page, $limit)->field($field)->withoutField($outfield)->select()->toArray();
        $count = self::where($where)->count();
        return [$list, $count];
    }
}
