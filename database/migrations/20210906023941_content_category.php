<?php

use think\migration\Migrator;
use think\migration\db\Column;

class ContentCategory extends Migrator
{
    /**
     * 内容分类表 默认InnoDB 表名自带表前缀 自动创建id字段，
     */
    public function change() {
        $table = $this->table('content_category');
        $table->addColumn('pid', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '文章分类父id'))
            ->addColumn('cate_name', 'string', array('limit' => 50, 'default' => '', 'comment' => '分类名称'))
            ->addColumn('status', 'boolean', array('limit' => 1, 'default' => 1, 'comment' => '1正常0禁用'))
            ->addIndex(array('id'), array('unique' => true))
            ->setComment('内容分类表')
            ->create();
    }
}
