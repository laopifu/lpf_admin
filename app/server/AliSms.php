<?php
/**
 * 阿里云短信
 */

namespace app\server;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

class AliSms {
    public function sendCode ($mobile = '', $json_array = '', $smsCode = '') {
        $alibabaCloud = new AlibabaCloud();
        $alibabaCloud->accessKeyClient('***************', '******************')//key和另一个
        ->regionId('cn-hangzhou')
            ->asDefaultClient();

        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId'      => "cn-hangzhou",
                        'PhoneNumbers'  => $mobile,         //手机号
                        'SignName'      => "签名", //签名
                        'TemplateCode'  => $smsCode,        //短信模板
                        'TemplateParam' => $json_array,  //短信变量名：变量值
                    ],
                ])
                ->request();
            return $result;
        } catch (ClientException $e) {
            return error($e->getErrorMessage());
//            echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            return error($e->getErrorMessage());
//            echo $e->getErrorMessage() . PHP_EOL;
        }
    }
}
