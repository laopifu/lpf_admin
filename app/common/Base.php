<?php

/**
 * 所有模块基础
 */

namespace app\common;

use app\BaseController;
use app\model\MasterModel;
use app\server\AliSms;
use app\server\Excel;
use app\server\Mailer;
use PhpOffice\PhpSpreadsheet\IOFactory;
use think\App;
use think\facade\Validate;
use think\Request;

class Base extends BaseController {
    protected $Time;
    protected $assign;

    public function __construct(App $app = null) {
        parent::__construct($app);
        //变量赋值，返回
        $this->Time = time();//全局时间戳
    }

    /**
     * excel导入
     * @param Request $request
     * @return \think\response\Json
     */
    public function uploadExcel() {
        //获取上传文件
        $file = $this->request->file('excel');
        //获取额外参数
        $postData = $this->request->post();
        $postData['excel'] = $file;
        //验证文件及其他参数
        $validate = Validate::rule([
            "excel|文件" => ['require', 'filesize' => 1024000, 'fileExt' => ['xls', 'xlsx']]
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }
        $excel = new excel();
        $data = $excel->importExcel($file);

        return success();
    }

    /**
     * 发送邮箱信息
     */
    public function send_mail() {
        $mail = new mailer();
        $mail->sendMail('915120623@qq.com');
    }

    /**
     * 导出数据
     */
    public function order_export() {//导出Excel
        //数据库中的数据表
        $xlsName = "用户列表";
        $xlsCell = [
            ['openid', 'openid'],
            ['nickname', '昵称'],
            ['headimg', '头像'],
            ['login_time', '登陆时间'],
            ['reg_time', '注册时间'],
            ['status', '状态'],
        ];

        $xlsData = MasterModel::select();
        /* 调用导出方法 */
        $excel = new excel();
        $excel->exportExcel($xlsName, $xlsCell, $xlsData);
    }

    /**
     * 发送短信通知
     */
    public function send_sms() {
        $json_array = [
            'order_no' => 123456,
            'user_email' => '915120623@qq.com',
            'time' => date('Y-m-d H:i:s')
        ];
        $json_array = json_encode($json_array);
        $ali_code = new aliSms();
        $result = $ali_code->sendCode(15127190730, $json_array, 'SMS_215798059');
        dump($result);
    }
}
