<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AuthRule extends Migrator {
    /**
     * 权限规则表 默认InnoDB 表名自带表前缀 自动创建id字段，
     */
    public function change() {
        $table = $this->table('auth_rule');
        $table->addColumn('parent_id', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '父菜单id'))
            ->addColumn('type', 'boolean', array('limit' => 1, 'default' => 1, 'comment' => '菜单类型;1:菜单+功能 2仅功能'))
            ->addColumn('name', 'string', array('limit' => 50, 'default' => '', 'comment' => '规则唯一英文标识,全小写'))
            ->addColumn('param', 'string', array('limit' => 50, 'default' => '', 'comment' => '菜单路径'))
            ->addColumn('title', 'string', array('limit' => 50, 'default' => '', 'comment' => '规则描述'))
            ->addColumn('icon', 'string', array('limit' => 20, 'default' => '', 'comment' => '图标'))
            ->addColumn('sort', 'integer', array('limit' => 10, 'default' => 1, 'comment' => '排序'))
            ->addColumn('status', 'boolean', array('limit' => 1, 'default' => 1, 'comment' => '是否有效(0:无效,1:有效)'))
            ->addIndex(array('id'), array('unique' => true))
            ->addIndex(array('name'), array('unique' => true))
            ->setComment('权限规则表')
            ->create();
        /*填充默认数据*/
        $rows = [
            [
                "parent_id" => 0,
                "status" => 1,
                "type" => 1,
                "name" => "/master/index/index",
                "param" => "#/home/index",
                "title" => "首页",
                "icon" => "layui-icon-home",
                "sort" => 1,
            ]
            ,
            [
                "parent_id" => 0,
                "status" => 1,
                "type" => 1,
                "name" => "/master/master/default",
                "param" => "",
                "title" => "管理员管理",
                "icon" => "layui-icon-user",
                "sort" => 1
            ],
            [
                "parent_id" => 2,
                "status" => 1,
                "type" => 1,
                "name" => "/master/master/master_list",
                "param" => "#/master/master",
                "title" => "管理员列表",
                "icon" => "",
                "sort" => 2,
            ],
            [
                "parent_id" => 3,
                "status" => 0,
                "type" => 2,
                "name" => "/master/master/daochulist",
                "param" => "",
                "title" => "导出管理员数据",
                "icon" => "",
                "sort" => 3,
            ],
            [
                "parent_id" => 3,
                "status" => 1,
                "type" => 2,
                "name" => "/master/master/master_add",
                "param" => "",
                "title" => "创建管理员",
                "icon" => "",
                "sort" => 3,
            ],
            [
                "parent_id" => 3,
                "status" => 1,
                "type" => 2,
                "name" => "/master/master/master_edit",
                "param" => "",
                "title" => "编辑管理员",
                "icon" => "",
                "sort" => 3,
            ]
            ,
            [
                "parent_id" => 2,
                "status" => 1,
                "type" => 1,
                "name" => "/master/role/role_list",
                "param" => "#/master/role",
                "title" => "角色列表",
                "icon" => "",
                "sort" => 1
            ],
            [
                "parent_id" => 7,
                "status" => 1,
                "type" => 2,
                "name" => "/master/role/role_add",
                "param" => "",
                "title" => "添加角色",
                "icon" => "",
                "sort" => 1
            ]
            ,
            [
                "parent_id" => 7,
                "status" => 1,
                "type" => 2,
                "name" => "/master/role/role_edit",
                "param" => "",
                "title" => "编辑角色",
                "icon" => "",
                "sort" => 1,
            ],
            [
                "parent_id" => 0,
                "status" => 1,
                "type" => 1,
                "name" => "/master/content/default",
                "param" => "",
                "title" => "内容管理",
                "icon" => "layui-icon-template",
                "sort" => 1,
            ],
            [
                "parent_id" => 10,
                "status" => 1,
                "type" => 1,
                "name" => "/master/content/content_list",
                "param" => "#/content/content_list",
                "title" => "内容列表",
                "icon" => "",
                "sort" => 1,
            ],
            [
                "parent_id" => 11,
                "status" => 0,
                "type" => 2,
                "name" => "/master/content/daochu",
                "param" => "",
                "title" => "内容导出",
                "icon" => "",
                "sort" => 1,
            ],
            [
                "parent_id" => 11,
                "status" => 1,
                "type" => 2,
                "name" => "/master/content/content_add",
                "param" => "",
                "title" => "添加内容",
                "icon" => "",
                "sort" => 1
            ],
            [
                "parent_id" => 11,
                "status" => 1,
                "type" => 2,
                "name" => "/master/content/content_edit",
                "param" => "",
                "title" => "编辑内容",
                "icon" => "",
                "sort" => 1,
            ],
            [
                "parent_id" => 7,
                "status" => 1,
                "type" => 2,
                "name" => "/master/role/role_set_rule",
                "param" => "",
                "title" => "设置管理员权限",
                "icon" => "",
                "sort" => 1,
            ],
            [
                "parent_id" => 23,
                "status" => 1,
                "type" => 1,
                "name" => "/master/system/get_log",
                "param" => "#/system/operating_log",
                "title" => "操作记录",
                "icon" => "",
                "sort" => 1
            ],
            [
                "parent_id" => 3,
                "status" => 1,
                "type" => 2,
                "name" => "/master/index/menu_list",
                "param" => "",
                "title" => "菜单列表",
                "icon" => "",
                "sort" => 1,
            ],
            [
                "parent_id" => 7,
                "status" => 1,
                "type" => 2,
                "name" => "/master/role/get_role_rule",
                "param" => "",
                "title" => "获取角色权限",
                "icon" => "",
                "sort" => 1,
            ],
            [
                "parent_id" => 3,
                "status" => 1,
                "type" => 2,
                "name" => "/master/master/master_status",
                "param" => "",
                "title" => "编辑管理员状态",
                "icon" => "",
                "sort" => 3,
            ],
            [
                "parent_id" => 11,
                "status" => 1,
                "type" => 2,
                "name" => "/master/content/get_category",
                "param" => "",
                "title" => "获取内容分类",
                "icon" => "",
                "sort" => 1
            ],
            [
                "parent_id" => 11,
                "status" => 1,
                "type" => 2,
                "name" => "/master/content/content_status",
                "param" => "",
                "title" => "编辑内容状态",
                "icon" => "",
                "sort" => 1,
            ],
            [
                "parent_id" => 7,
                "status" => 1,
                "type" => 2,
                "name" => "/master/role/role_status",
                "param" => "",
                "title" => "编辑角色状态",
                "icon" => "",
                "sort" => 1
            ],
            [
                "parent_id" => 0,
                "status" => 1,
                "type" => 1,
                "name" => "/master/system/default",
                "param" => "",
                "title" => "系统管理",
                "icon" => "layui-icon-set",
                "sort" => 1,
            ],
            [
                "parent_id" => 10,
                "status" => 1,
                "type" => 1,
                "name" => "/master/contentcate/cate_list",
                "param" => "#/content/content_cate",
                "title" => "分类列表",
                "icon" => "",
                "sort" => 1
            ],
            [
                "parent_id" => 24,
                "status" => 1,
                "type" => 2,
                "name" => "/master/contentcate/cate_add",
                "param" => "",
                "title" => "分类添加",
                "icon" => "",
                "sort" => 1
            ]
        ];
        $this->table('auth_rule')->insert($rows)->save();
    }
}
