# 老匹夫rbac+简单聊天

#### 环境要求
1. ThinkPHP6.0
2. Mysql5.6
3. PHP7.3

#### 方法

- [x] 获取数据验证方法
- [x] Jwt生成token->中间件验证方法
- [x] 文件，图片上传方法，上传到阿里云，七牛云
- [ ] 微信支付 （暂定）
- [ ] 支付宝支付  （暂定）
- [x] 邮件发送
- [x] 短信发送
- [x] 接口数据加密
- [X] 长链接（消息推送，workman）
- [X] 方法命名规范（写法规范，model,controller书写规范）
- [x] 数据返回公共方法
- [ ] 第三方登录（微信，QQ）（暂定）
- [x] 日志，操作日志
- [X]  权限
- [ ] 增，删，改，查的不同的书写规范
- [ ]  二维码生成  （暂定）
- [x] 表格excel数据导入
#### 文件、控制器命名规则：（驼峰或下划线）

 1. app/api/controller  用作给第三方接口
 2. app/common  公共控制器
- Base.php   所有控制器基础
- Master.php 后台接口控制器基础
 3. app/master  后台应用
- config            单应用配置文件
- controller        控制器
- route             路由定义
 4. app/middleware  中间件文件
 5. app/model  全部应用的数据库模型（数据库名+Model.php）
 6. app/server  全部应用所用到的第三方服务类
#### 使用说明
一、准备工作
1.首先执行composer update 更新composer包
2.复制并修改/.example.env文件内容 并将修改后的文件放入新建的.env中
3.如果部署到服务器的话记得 把runtime权限777、/public/storage权限777
二、
运行数据库迁移文件和数据填充
~~~
php think migrate:run
~~~
备注：创建数据迁移
~~~
php think migrate:create TableName
~~~
三、
聊天室运行
~~~
php think worker:server
或
php think worker:server -d  //守护进程运行
~~~
在linux运行时可能会执行报错
需要修改php.ini将disable_functions这个里面的以pcntl开头开头的函数都去掉，然后保存，接着重新启动就好了
如果需要更新框架使用
~~~
composer update topthink/framework
~~~
更新日志
注意tp新框架6.1之后没有filesystem ，需要手动加一下
~~~
composer require topthink/think-filesystem 1.0.1
~~~
注意tp新框架6.1.3路由插件有问题 ，需要手动写死6.1.1 在composer.json第4行 修改为"topthink/framework": "6.1.1"
~~~
"require": {
        "php": ">=7.1.0",
        "topthink/framework": "6.1.1",<-在这里
        "topthink/think-orm": "^2.0",
        "topthink/think-multi-app": "^1.0",
~~~
