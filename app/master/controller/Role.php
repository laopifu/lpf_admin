<?php


namespace app\master\controller;


use app\common\Base;
use app\model\RoleModel;
use app\model\AuthRuleModel;
use think\facade\Validate;

class Role extends Base {

    /**
     * 角色列表
     * @return \think\response\Json
     */
    public function role_list() {
        list($where, $page, $limit) = getData();
        $field='';
        if(isset($where['field'])){
            $field=$where['field'];
            unset($where['field']);
        }
        list($list, $count) = RoleModel::getList($where, $page, $limit, $field, '');
        return success('查看角色列表', $list, $count);
    }

    /**
     * 添加角色
     * @return \think\response\Json
     */
    public function role_add() {
        $postData = $this->request->post([
            'name' => '',
            'remark' => '',
            'status' => 0,
        ]);
        $validate = Validate::rule([
            "name|角色名" => "require|min:5",
            "remark|备注" => "max:20",
            "status|账号状态" => "integer"
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }
        $postData['create_time'] = $this->Time;
        RoleModel::insert($postData);
        return success();
    }

    /**
     * 编辑角色
     * @return \think\response\Json
     */
    public function role_edit() {
        $postData = $this->request->post([
            'id',
            'name' => '',
            'remark' => '',
            'status' => 0,
        ]);
        $validate = Validate::rule([
            "id|数据标识" => "require|integer|notIn:1",
            "name|角色名" => "require|min:5",
            "remark|备注" => "max:20",
            "status|账号状态" => "integer"
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }
        $postData['update_time'] = $this->Time;
        RoleModel::update($postData);
        return success('编辑角色');
    }
    /**
     * 编辑角色状态
     * @return \think\response\Json
     */
    public function role_status() {
        $postData = $this->request->post([
            'id',
            'status' => 0,
        ]);
        $validate = Validate::rule([
            "id|数据标识" => "require|integer|notIn:1",
            "status|账号状态" => "integer"
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }
        $postData['update_time'] = $this->Time;
        RoleModel::update($postData);
        return success('编辑角色');
    }

    /**
     * 获取权限树
     * @return \think\response\Json
     */
    public function get_role_rule() {
        $postData = $this->request->get([
            'role_id'
        ]);
        $role = RoleModel::where('id', $postData['role_id'])->value('rule_id');
        $role_arr = explode(',', $role);
        $rule = AuthRuleModel::where(['status' => 1])->field('id,parent_id,title')->select()->toArray();
        $rule_list_arr = $this->getTree($rule, $role_arr);
        return success('权限树', $rule_list_arr);
    }

    /**
     * 获取带状态权限树
     * @param $array
     * @param $role_arr
     * @param int $parent_id
     * @return array
     */
    function getTree($array, $role_arr, $parent_id = 0) {
        $menu_list = [];
        $rule_list = arrayModel('parent_id', $parent_id, $array);
        foreach ($rule_list as $k => $v) {
            unset($array[$k]);
            $v['spread'] = true;
            $v['children'] = $this->getTree($array,$role_arr, $v['id']);
            if (empty($v['children'])&&in_array($v['id'], $role_arr)) {
                $v['checked'] = true;
            }
            $menu_list[] = $v;
        }
        return $menu_list;
    }

    /**
     * 设置角色权限
     */
    public function role_set_rule() {
        $postData = $this->request->post([
            'rule_ids'=>'',
            'role_id'
        ]);
        $validate = Validate::rule([
            "role_id|数据标识" => "require|integer|notIn:1",
            "rule_ids|规则" => "require"
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }
        RoleModel::where('id', $postData['role_id'])->update(['rule_id' => $postData['rule_ids']]);
        return success('设置成功');
    }
}
