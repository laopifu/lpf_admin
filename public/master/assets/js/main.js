layui.config({
    version: '2.0.2',   // 更新组件缓存，设为true不缓存，也可以设一个固定值
    base: 'assets/module/'
}).extend({
    steps: 'steps/steps',
    notice: 'notice/notice',
    cascader: 'cascader/cascader',
    // dropdown: 'dropdown/dropdown',
    fileChoose: 'fileChoose/fileChoose',
    Split: 'Split/Split',
    Cropper: 'Cropper/Cropper',
    tagsInput: 'tagsInput/tagsInput',
    citypicker: 'city-picker/city-picker',
    introJs: 'introJs/introJs',
    zTree: 'zTree/zTree'
}).use(['layer', 'setter', 'index', 'admin'], function () {
    var $ = layui.jquery;
    var layer = layui.layer;
    var setter = layui.setter;
    var index = layui.index;
    var admin = layui.admin;

    /* 检查是否登录 */
    if (!setter.getToken()) {
        layer.msg('未检测登录信息，请重新登陆')
        return location.replace(setter.baseLogin);
    }

    /* 获取用户信息 */
    admin.req('/common/get_master_info', function (res) {
        if (200 === res.code) {
            setter.putUser(res.data);
            admin.renderPerm();  // 移除没有权限的元素
            $('#huName').text(res.data.nickname);
            if (res.data.avatar) {
                $('#huName').prev().attr('src', res.data.avatar);
            }
            get_menu_list();
        }
    });

    function get_menu_list() {
        /* 加载侧边栏 */
        admin.req('/common/get_menu_list', function (res) {
            if (200 === res.code) {
                index.regRouter(res.data, function (data) {
                    data.name = data.title;
                    data.url = data.param;
                    data.iframe = data.component;
                    data.show = !data.hide;
                    data.subMenus = data.children;
                    return data;
                });  // 注册路由
                index.renderSide(res.data);  // 渲染侧边栏
                // 加载主页
                index.loadHome({
                    url: '#/home/index',
                    name: '<i class="layui-icon layui-icon-home"></i>'
                });
            } else {
                layer.msg('获取菜单列表失败', {icon: 2, anim: 6});
            }
        });
    }


});
