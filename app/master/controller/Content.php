<?php


namespace app\master\controller;

use app\common\Base;
use app\model\ContentCategoryModel;
use app\model\ContentModel;
use app\server\Upload;
use think\facade\Validate;

class Content extends Base {

    /**
     * 内容列表
     * @return \think\response\Json|\think\response\View
     */
    public function content_list() {
        list($where, $page, $limit) = getData();
        $whereLike = [];
        if (isset($where['title'])) {
            $whereLike[] = ['title', 'like', '%' . $where['title'] . '%'];
            unset($where['title']);
        }
        list($list, $count) = ContentModel::getList($where, $page, $limit, $whereLike);
        return success('ok', $list, $count);
    }

    /**
     * 获取文章分类
     */
    public function get_category() {
        $list = ContentCategoryModel::getSelectList();
        return success('ok', $list);
    }

    /**
     * 添加内容
     * @return \think\response\Json|\think\response\View
     */
    public function content_add() {
        list($code,$data,$msg) = $this->verifyData([
            ['title|标题',"require|min:5"],
            ['remark|描述',"require"],
            ['images|图片',"require"],
            ['cate_id|分类',"require|integer"],
            ['content|文章内容',"require"],
            ['create_time',"require",$this->Time]
        ]);
        if ($code===0) {
            return error($msg);
        }
        ContentModel::insert($data);
        return success('添加内容成功');
    }

    /**
     * 编辑内容
     * @return \think\response\Json
     */
    public function content_edit() {
        list($code,$data,$msg) = $this->verifyData([
            ['title|标题',"require|min:5"],
            ['remark|描述',"require"],
            ['images|图片',"require"],
            ['id|数据标识',"require|integer"],
            ['cate_id|分类',"require|integer"],
            ['content|文章内容',"require"],
            ['create_time',"require",$this->Time]
        ]);
        if ($code===0) {
            return error($msg);
        }
        ContentModel::update($data);
        return success('编辑内容成功');
    }

    /**
     * 处理内容状态
     * @return \think\response\Json
     */
    public function content_status() {
        list($code,$data,$msg) = $this->verifyData([
            ['is_del|删除情况',"integer"],
            ['status|展示情况',"integer"],
            ['id|数据标识',"require|integer"]
        ]);
        if ($code===0) {
            return error($msg);
        }
        if (isset($data['status']) || isset($data['is_del'])) {
            ContentModel::update($data);
            return success('编辑内容成功');
        }
        return error('缺少参数');
    }
}
