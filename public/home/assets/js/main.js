layui.config({
    version: '2.0.2',   // 更新组件缓存，设为true不缓存，也可以设一个固定值
    base: 'assets/module/'
}).extend({
    notice: 'notice/notice',
    // dropdown: 'dropdown/dropdown',
}).use(['jquery', 'layRouter','setter'], function() {
    var $ = layui.jquery;
    var layRouter = layui.layRouter;

    layRouter.reg(['#/home/index', '#/content/index', '#/order/introduction'], function(routerInfo) {
        var path = routerInfo.path.join('/');
        $('#LAY_MAIN').load('view/' + path + '.html');
    });
    layRouter.init({
        index: '#/home/index'
    });
});
