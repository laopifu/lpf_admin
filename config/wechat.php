<?php

return [
    'app_id' => env('wechat.app_id', ''),
    'secret' => env('wechat.app_secret', ''),
    // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
    'token'=>'laopifu',
    'response_type' => 'array',
    'oauth' => [
        'scopes'   => ['snsapi_userinfo'],
        'callback' => '/api/login/getLoginUser',
    ],
];
