<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Master extends Migrator
{
    /**
     * 后台管理员表 默认InnoDB 表名自带表前缀 自动创建id字段，
     * $this->table('master', ['id' => 'uid']);//重命名主键
     */
    public function change() {
        $table = $this->table('master', ['id' => 'uid']);
        $table->addColumn('rid', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '管理员角色id'))
            ->addColumn('nickname', 'string', array('limit' => 20, 'default' => '', 'comment' => '昵称'))
            ->addColumn('avatar', 'string', array('limit' => 100, 'default' => '', 'comment' => '头像'))
            ->addColumn('username', 'string', array('limit' => 20, 'default' => '', 'comment' => '账号'))
            ->addColumn('password', 'string', array('limit' => 40, 'default' => '', 'comment' => '密码'))
            ->addColumn('login_time', 'string', array('limit' => 10, 'default' => '', 'comment' => '登陆时间'))
            ->addColumn('reg_time', 'string', array('limit' => 10, 'default' => '', 'comment' => '注册时间'))
            ->addColumn('remark', 'string', array('limit' => 10, 'default' => '', 'comment' => '备注'))
            ->addColumn('status', 'boolean', array('limit' => 1, 'default' => 1, 'comment' => '1正常0禁用'))
            ->addIndex(array('uid'), array('unique' => true))
            ->setComment('后台管理员表')
            ->create();
        /*添加默认数据*/
        $rows = [
            [
                'rid' => 1,
                'nickname' => '老匹夫',
                'avatar' => 'http://admin.laopifu.top/master/assets/images/head.png',
                'username' => 'laopifu',
                'password' => 'a1f100d1d3854923f5d043c9b04503f5',
                'login_time' => '',
                'reg_time' => time(),
                'remark' => '大管理',
                'status' => 1,
            ],
            [
                'rid' => 1,
                'nickname' => '小匹夫',
                'avatar' => 'http://admin.laopifu.top/master/assets/images/head.png',
                'username' => 'admin',
                'password' => 'd5f49a77370c05b92f1c4a6969351460',
                'login_time' => '',
                'reg_time' => time(),
                'remark' => '小管理',
                'status' => 1,
            ]
        ];
        $this->table('master')->insert($rows)->save();
    }
}
