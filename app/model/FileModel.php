<?php

namespace app\model;

use think\Model;

class FileModel extends Model {
    protected $pk = 'id';

    protected $name = 'file';
    // 设置字段信息
    protected $type = [

    ];

    /**
     * @param array $where
     * @param int $page
     * @param int $limit
     * @param string $field
     * @param string $outfield
     * @return array
     */
    public static function getList(array $where, int $page, int $limit, string $field = '',string $outfield = '') {
        $list = self::where($where)->page($page, $limit)->field($field)->withoutField($outfield)->select()->toArray();
        $count = self::where($where)->count();
        return [$list, $count];
    }

}
