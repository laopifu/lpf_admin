<?php
/**
 *
 */
namespace app\api\controller;

use app\model\ContentModel;
use app\server\FreeSwitchEsl;

class Index {
    public function test() {
        $freeswitch = new FreeSwitchEsl();
        $originate = sprintf("originate {origination_uuid=%s}user/%s &bridge({origination_uuid=%s}user/%s)",
            1,1017,2,1017
        );
        $version=$freeswitch->bgapi($originate);
//        $version = $freeswitch->api("show api");
        dd($version);
    }
    public function get_content_list(){
        list($where, $page, $limit) = getData();
        $where['is_del']=0;
        $where['c.status']=1;
        $whereLike = [];
        list($list, $count) = ContentModel::getList($where, $page, $limit, $whereLike);
        return success('ok', $list, $count);
    }
}
