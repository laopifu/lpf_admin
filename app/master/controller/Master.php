<?php
/**
 * 后台账号
 */

namespace app\master\controller;

use app\common\Base;
use app\model\MasterModel;
use app\model\RoleModel;
use app\server\Redis;
use think\facade\Validate;

class Master extends Base {

    /**
     * 管理员列表数据
     * @return \think\response\Json|\think\response\View
     */
    public function master_list() {
        list($where, $page, $limit) = getData();
        list($list, $count) = MasterModel::getList($where, $page, $limit, '', 'password');
        $Role_list = RoleModel::where('id', '>', 0)->column('name', 'id');
        foreach ($list as $k => $v) {
            $list[$k]['role_name'] = $Role_list[$v['rid']];
        }
        return success('查看管理员列表数据', $list, $count);
    }

    /**
     * 管理员添加
     * @return \think\response\Json|\think\response\View
     */
    public function master_add() {
        $postData = $this->request->post([
            'username'=>'',
            'password'=>'',
            'nickname'=>'',
            'remark'=>'',
            'rid',
            'status' => 0,
        ]);
        $validate = Validate::rule([
            "username|用户名" => "require|min:5",
            "password|密码" => "require|min:6",
            "nickname|昵称" => "require|max:8",
            "remark|备注" => "max:20",
            "rid|角色" => "require|integer",
            "status|账号状态" => "integer"
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }
        if (MasterModel::where('username', $postData['username'])->find()) {
            return error('用户名重复');
        }
        $postData['reg_time'] = $this->Time;
        $uid = MasterModel::insertGetId($postData);
        $password = makePassword($postData['password'], $uid);
        MasterModel::where('uid', $uid)->update(['password' => $password]);
        return success('添加管理员');
    }

    /**
     * 管理员编辑
     * @return \think\response\Json|\think\response\View
     */
    public function master_edit() {
        $postData = $this->request->post([
            'username'=>'',
            'password',
            'nickname'=>'',
            'remark'=>'',
            'rid',
            'uid',
        ]);
        $validate = Validate::rule([
            "username|用户名" => "require|min:5",
            "password|密码" => "min:6",
            "nickname|昵称" => "require|max:8",
            "remark|备注" => "max:20",
            "rid|角色" => "require|integer",
            "uid|数据标识" => "require|integer|notIn:1,2"
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }
        $where = [];
        $where[] = ['username', '=', $postData['username']];
        $where[] = ['uid', '<>', $postData['uid']];
        if (MasterModel::where($where)->find()) {
            return error('用户名重复');
        }
        if (!empty($postData['password'])) {
            $postData['password'] = makePassword($postData['password'], $postData['uid']);
        } else {
            unset($postData['password']);
        }
        MasterModel::where('uid', $postData['uid'])->update($postData);
        $redis = new Redis();
        $redis->delToken($postData['uid']);
        return success('编辑管理员');
    }

    /**
     * 编辑角色状态
     * @return \think\response\Json
     */
    public function master_status(): \think\response\Json {
        $postData = $this->request->post([
            'status' => 0,
            'uid' => 0,
        ]);
        $validate = Validate::rule([
            "status|账号状态" => "require|integer",
            "uid|数据标识" => "require|integer|notIn:1,2"
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }
        MasterModel::update($postData);
        $redis = new Redis();
        $redis->delToken($postData['uid']);
        return success('编辑角色状态成功');
    }
}
