<?php
declare (strict_types=1);

namespace app\middleware;

use app\server\Redis;

class CheckWxLogin {
    /**
     * 仅验证登录
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next) {
        $token = trim(ltrim($request->header('Authentication'), 'Bearer'))??'';
        if ($token) {
            //验证登录
            $personal_info = verifyJwt($token);//token验证
            if ($personal_info['code'] == 0) {
                return error($personal_info['msg'],401);
            }
            $redis = new Redis();
            $personal = $redis->getWechatToken($personal_info['data']['openid']);
            if (empty($personal)) {
                return error('登录状态失效，请重新登录',401);
            }
            $request->personal = $personal_info['data'];
        } else {
            return error('登录信息失效，请重新登录',401);
        }
        return $next($request);
    }
}
