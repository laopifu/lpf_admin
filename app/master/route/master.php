<?php

use think\facade\Route;
use app\middleware\CheckLogin;
use app\middleware\CheckAuth;

Route::group('login',function () {
    Route::post('doLogin', 'master/login/doLogin');//登录
});

Route::group('common',function () {
    Route::post('logout', 'master/login/logout');//退出登录
    Route::get('get_master_info', 'master/common/get_master_info');//获取登录信息
    Route::get('get_menu_list', 'master/common/get_menu_list');//获取权限菜单
    Route::post('upLoad_file', 'master/common/upLoad_file');//文件上传
    Route::post('upLoad_image', 'master/common/upLoad_image');//编辑器文件上传
    Route::get('get_file_list', 'master/common/get_file_list');//文件列表
})->middleware([CheckLogin::class]);

Route::group('system',function () {
    Route::get('get_log', 'master/system/get_log');//操作记录列表
})->middleware([CheckLogin::class,CheckAuth::class]);

Route::group('master',function () {
    Route::get('master_list', 'master/master/master_list');//用户列表
    Route::post('master_add', 'master/master/master_add');//用户添加
    Route::post('master_edit', 'master/master/master_edit');//用户编辑
    Route::post('master_status', 'master/master/master_status');//状态
})->middleware([CheckLogin::class,CheckAuth::class]);

Route::group('role',function () {
    Route::get('role_list', 'master/role/role_list');//权限列表
    Route::post('role_add', 'master/role/role_add');//权限添加
    Route::post('role_edit', 'master/role/role_edit');//权限编辑
    Route::get('get_role_rule', 'master/role/get_role_rule');//获取权限树
    Route::post('role_set_rule', 'master/role/role_set_rule');//设置角色权限
    Route::post('role_status', 'master/role/role_status');//编辑角色状态
})->middleware([CheckLogin::class,CheckAuth::class]);

Route::group('content',function () {
    Route::get('content_list', 'master/content/content_list');//文章列表
    Route::post('content_status', 'master/content/content_status');//文章状态修改
    Route::post('content_add', 'master/content/content_add');//文章添加
    Route::post('content_edit', 'master/content/content_edit');//文章编辑
    Route::get('get_category', 'master/content/get_category');//文章分类列表
})->middleware([CheckLogin::class,CheckAuth::class]);
Route::group('content_cate',function () {
    Route::get('cate_list', 'master/contentCate/cate_list');//列表
//    Route::post('content_status', 'master/content/content_status');//文章状态修改
    Route::post('cate_add', 'master/contentCate/cate_add');//文章添加
    Route::post('cate_edit', 'master/contentCate/cate_edit');//文章编辑
//    Route::get('get_category', 'master/content/get_category');//文章分类列表
})->middleware([CheckLogin::class,CheckAuth::class]);
