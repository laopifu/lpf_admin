<?php

namespace app\model;

use think\Model;

class AuthRuleModel extends Model {

    protected $pk = 'id';

    protected $name = 'auth_rule';

    public static function getList(array $where, int $page, int $limit, string $field = '',string $outfield = '') {
        $list = self::where($where)->page($page, $limit)->field($field)->withoutField($outfield)->select()->toArray();
        $count = self::where($where)->count();
        return [$list, $count];
    }
}