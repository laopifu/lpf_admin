<?php

namespace app\model;

use think\Model;

class MasterModel extends Model {
    protected $pk = 'uid';

    protected $name = 'master';
    // 设置字段信息
    protected $type = [
        'login_time' => 'timestamp',
        'reg_time' => 'timestamp',
    ];

    /**
     * @param array $where
     * @param int $page
     * @param int $limit
     * @param string $field
     * @param string $outfield
     * @return array
     */
    public static function getList(array $where, int $page, int $limit, string $field = '',string $outfield = '') {
        $list = self::where($where)->page($page, $limit)->field($field)->withoutField($outfield)->select()->toArray();
        $count = self::where($where)->count();
        return [$list, $count];
    }

}
