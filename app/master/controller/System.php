<?php

namespace app\master\controller;

use app\model\MasterModel;
use app\server\Redis;
use think\App;
use app\common\Base;
use think\facade\Cache;
use think\facade\Log;
use think\facade\Validate;

class System extends Base {

    /**
     * 获取操作日志
     */
    public function get_log() {
        $getData = $this->request->get([
            'time' => date('Y-m-d'),
            'page' => 1,
            'limit' => 20
        ]);
        $validate = Validate::rule([
            'time' => 'require|dateFormat:Y-m-d'
        ]);
        if (!$validate->check($getData)) {
            return error($validate->getError());
        }
        $path = \think\facade\App::getRuntimePath() . 'operation_log';
        $time = explode('-', $getData['time']);
        $yearMonth = $time[0] . $time[1];
        $path .= '/' . $yearMonth . '/' . $time[2] . '.log';
        if (!file_exists($path)) {
            return error($getData['time'] . '没有操作记录');
        }
        $data = file_get_contents($path);
        $data = explode(PHP_EOL, $data);
        $logData = [];
        foreach ($data as &$v) {
            if (!empty($v)) {
                $v = json_decode($v, true);
                $logData[] = array_merge(['time' => $v['time']], json_decode($v['msg'], true));
            }
        }
        $count=count($logData);
        $logData = array_slice($logData, (intval($getData['page'])-1) * intval($getData['limit']), $getData['limit']);
        return success('读取日志', $logData,$count);
    }
}
