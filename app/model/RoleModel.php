<?php

namespace app\model;

use think\Model;

class RoleModel extends Model {

    protected $pk = 'id';

    protected $name = 'role';

    public static function getList(array $where, int $page, int $limit, string $field = '',string $outfield = '') {
        $list = self::where($where)->page($page, $limit)->field($field)->withoutField($outfield)->select()->toArray();
        $count = self::where($where)->count();
        return [$list, $count];
    }
//    public static function onAfterRead($role): void {
//        echo '读取:'.$role->id;
//    }
//    public static function onAfterInsert(): void {
//        echo '添加';
//    }
//    public static function onAfterUpdate(): void {
//        echo '更新';
//    }
//    public static function onAfterDelete(): void {
//        echo '删除';
//    }
}