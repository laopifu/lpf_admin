<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Content extends Migrator
{
    /**
     * 内容表 默认InnoDB 表名自带表前缀 自动创建id字段，
     */
    public function change() {
        $table = $this->table('content');
        $table->addColumn('cate_id', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '文章分类id'))
            ->addColumn('title', 'string', array('limit' => 50, 'default' => '', 'comment' => '标题'))
            ->addColumn('remark', 'string', array('limit' => 100, 'default' => '', 'comment' => '备注'))
            ->addColumn('images', 'string', array('limit' => 100, 'default' => '', 'comment' => '图片'))
            ->addColumn('content', 'text', array('comment' => '内容'))
            ->addColumn('create_time', 'string', array('limit' => 10, 'default' => '', 'comment' => '添加时间'))
            ->addColumn('status', 'boolean', array('limit' => 1, 'default' => 1, 'comment' => '1正常0禁用'))
            ->addColumn('is_del', 'boolean', array('limit' => 1, 'default' => 0, 'comment' => '是否删除 1是0否'))
            ->addIndex(array('id'), array('unique' => true))
            ->addIndex(array('title'), array('unique' => true))
            ->setComment('内容表')
            ->create();
    }
}
