<?php

return [
    // 微信支付必要配置
    'app_id' => env('wechat.app_id', ''),
    'mch_id' => env('wxpay.mch_id', ''),//商户号
    'key' => env('wxpay.key', ''),// API 密钥

    // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
//    'cert_path'          => 'path/to/your/cert.pem', // XXX: 绝对路径！！！！
//    'key_path'           => 'path/to/your/key',      // XXX: 绝对路径！！！！

    'notify_url'         => request()->domain().'/api/wxpay/notify',     // 你也可以在下单时单独设置来想覆盖它
];
