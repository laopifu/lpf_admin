<?php

namespace app\model;

use think\facade\Db;
use think\Model;

class ContentModel extends Model {

    protected $pk = 'id';

    protected $name = 'content';
    // 设置字段信息
    protected $schema = [
        'id' => 'int',
        'cate_id' => 'int',
        'title' => 'string',
        'remark' => 'string',
        'images' => 'string',
        'content' => 'string',
        'create_time' => 'int',
        'status' => 'int',
        'is_del' => 'int',

    ];

    /**
     * 列表查询
     * @param array $where
     * @param string $field
     * @return array
     */
    public static function getList(array $where, $page, $limit,$whereLike) {
        $mod = self::alias('c')->leftJoin('lpf_content_category cc', 'c.cate_id=cc.id')->where($where)->where($whereLike);
        $list = $mod->page($page, $limit)->field('cc.cate_name,c.*')->select()->toArray();
        $count = $mod->count();
        return [$list, $count];
    }
}
