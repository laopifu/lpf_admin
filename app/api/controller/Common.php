<?php


namespace app\api\controller;

use app\common\Base;
use app\model\AuthRuleModel;
use app\model\FileModel;
use app\model\RoleModel;
use app\server\Redis;
use app\server\Upload;
use think\facade\Validate;

class Common extends Base {

    /**
     * 获取登录信息
     */
    public function get_master_info() {
        return success('获取登录用户信息', $this->request->personal);
    }
}
