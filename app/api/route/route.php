<?php

use think\facade\Route;
use app\middleware\CheckWxLogin;

Route::get('/', function () {
    return redirect('/home');
});
Route::group('login',function () {
    Route::get('test', 'index/test');
    Route::get('xnLogin', 'api/login/xnLogin');//假登录
    Route::get('getWxCode', 'api/login/getWxCode');//登录获取code
    Route::get('getLoginUser', 'api/login/getLoginUser');//登录保存用户信息

    Route::get('getQrcode', 'api/login/getQrcode');//获取二维码
    Route::get('scanLogin', 'api/login/scanLogin');//扫码登录

});
Route::group('index',function () {
    Route::get('get_content_list', 'api/index/get_content_list');
});
Route::group('wxpay',function () {
    Route::post('makeOrder', 'api/wxpay/makeOrder')->middleware(CheckWxLogin::class);//统一下单
    Route::post('checkOrder', 'api/wxpay/checkOrder')->middleware(CheckWxLogin::class);//轮询查询订单状态
    Route::any('notify', 'api/wxpay/notify');//支付回调
});
