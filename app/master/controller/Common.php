<?php


namespace app\master\controller;

use app\common\Base;
use app\model\AuthRuleModel;
use app\model\FileModel;
use app\model\RoleModel;
use app\server\Redis;
use app\server\Upload;
use think\facade\Validate;

class Common extends Base {

    /**
     * 获取登录信息
     */
    public function get_master_info() {
        return success('获取登录用户信息', $this->request->master);
    }

    /**
     * 获取登录用户的菜单
     */
    public function get_menu_list() {
        //验证权限
        $role = RoleModel::where('id', $this->request->master['rid'])->find();
        if (empty($role)) {
            return error('角色不存在');
        }
        if ($role['status'] == 0) {
            return error('角色被禁用');
        }
        //从redis获取全部正常状态的规则列表
        //Todo 后台菜单添加完成后开启读取redis菜单数据
        $redis = new Redis();
//        $authRuleRedis=$redis->getAuthRule();
//        if($authRuleRedis!==false&&!empty($authRuleRedis)){
//            $authRule=json_decode($authRuleRedis,true);
//        }else{
        $authRule = AuthRuleModel::where('status', 1)->order('parent_id asc,sort asc')->select()->toArray();
        $redis->setAuthRule(json_encode($authRule));
//        }
        $rule_id = $role['rule_id'];
        $authRule=arrayModel('type',1,$authRule);
        $hasRule = arrayInModel('id', $rule_id, $authRule);
        $sort = array_column($hasRule, 'sort');
        array_multisort($sort, SORT_ASC, $hasRule);
        $menu_list = $this->getTree($hasRule);
        return success('获取菜单列表', $menu_list);
    }

    private function getTree($array, $parent_id = 0) {
        $menu_list = [];
        $rule_list = arrayModel('parent_id', $parent_id, $array);
        foreach ($rule_list as $k=>$v) {
            unset($array[$k]);
            $v['children'] = $this->getTree($array, $v['id']);
            $menu_list[] = $v;
        }
        return $menu_list;
    }
    /**
     * 图片上传
     */
    public function upLoad_file() {
        $file = $this->request->file('file');
        $postData=[];
        $postData['file'] = $file;
        $validate = Validate::rule([
            'file' => 'require',
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }

        $upload = new Upload($file);
        $path = $upload->UpFile("storage/upload");
        FileModel::insert([
            'file_path' => $path,
            'file_name' => $file->getOriginalName()
        ]);
        return success('上传视频',['src'=>$path]);
    }
    /**
     * 编辑器图片上传
     */
    public function upLoad_image() {
        $file = $this->request->file('file');
        $postData=[];
        $postData['file'] = $file;
        $validate = Validate::rule([
            'file' => 'require',
        ]);
        if (!$validate->check($postData)) {
            return error($validate->getError());
        }
        $upload = new Upload($file);
        $path = $upload->UpFile("storage/upload");
        return success('上传图片',['src'=>$path]);
    }

    /**
     * 文件列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get_file_list() {
        $fileList = FileModel::select()->toArray();
        $list = [];
        foreach ($fileList as $value) {
            $list[] = [
                'url' => $value['file_path'],
                'name' => $value['file_name'],
                "isDir" => false
            ];
        }
        return success('获取文件列表', $list);
    }
}
