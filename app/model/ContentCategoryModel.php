<?php

namespace app\model;

use think\Model;

class ContentCategoryModel extends Model {
    protected $pk = 'id';

    protected $name = 'content_category';
    // 设置字段信息
    protected $type = [

    ];

    /**
     * 分类选择数据
     * @return array
     */
    public static function getSelectList() {
        $cateInfo = self::where(['status' => 1])->field('id,pid,cate_name')->order('pid')->select()->toArray();
        foreach ($cateInfo as &$v) {
            if ($v['pid'] == 0) {
                $v['disabled'] = true;
            }
        }
        return $cateInfo;
    }

    /**
     * 正常列表
     * @param array $where
     * @param $page
     * @param $limit
     * @return array
     */
    public static function getList(array $where, $page, $limit) {
        $list = self::where($where)->page($page, $limit)->select()->toArray();
        $count = self::where($where)->count();
        return [$list, $count];
    }

    public static function getTreeList($pid = 0) {
        $cate_list = self::where('pid', $pid)->select()->toArray();
        foreach ($cate_list as &$v) {
            $v['children'] = self::getTreeList($v['id']);
        }
        return $cate_list;
    }

}
