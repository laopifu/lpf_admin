<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Role extends Migrator
{
    /**
     * 角色表 默认InnoDB 表名自带表前缀 自动创建id字段，
     */
    public function change() {
        $table = $this->table('role');
        $table->addColumn('rule_id', 'string', array('limit' => 300, 'default' => 0, 'comment' => '规则id'))
            ->addColumn('name', 'string', array('limit' => 10, 'default' => '', 'comment' => '角色名称'))
            ->addColumn('remark', 'string', array('limit' => 50, 'default' => '', 'comment' => '备注'))
            ->addColumn('create_time', 'string', array('limit' => 10, 'default' => '', 'comment' => '创建时间'))
            ->addColumn('status', 'boolean', array('limit' => 1, 'default' => 1, 'comment' => '1正常0禁用'))
            ->addIndex(array('id'), array('unique' => true))
            ->setComment('角色表')
            ->create();
        /*填充默认数据*/
        $rows = [
            [
                'rule_id' => '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25',
                'create_time' => time(),
                'name' => '超级管理员',
                'remark' => '拥有网站最高管理员权限！',
                'status' => 1,
            ]
        ];
        $this->table('role')->insert($rows)->save();
    }
}
