<?php
/**
 * 本地上传或七牛云上传
 * $savePath=new Upload($file);
 * return success('ok',['src'=>$savePath->UpFile()]);
 */

namespace app\server;

use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use think\facade\Filesystem;

class Upload {
    protected $file;

    public function __construct ($file) {
        // 获取上传图片信息
        $this->file=$file;
    }

    /**
     * @return mixed
     */
    public function UpFile($path='') {
        if($path){
            return $this->LocalSave($path);
        }else{
            return $this->QiniuSave();
        }
    }

    /**
     * 本地上传
     * @return bool|string
     */
    public function LocalSave($path){
        $path= Filesystem::putFile($path, $this->file);
        $path = str_replace('\\', "/", $path);
        return request()->domain() . '/storage/' . $path;
    }

    /**
     * 七牛上传图片
     * @param $request
     */
    public function QiniuSave() {
        // 图片存储在本地的临时路经
        $filePath = $this->file->getRealPath();
        // 获取图片后缀
        $ext = $this->file->getOriginalExtension();
        // 上传到七牛后保存的新图片名
        $newImageName = date('Y') . '/' . date('m') . '/' . substr(md5($this->file->getOriginalName()), 0, 6)
            . date('YmdHis') . rand(00000, 99999) . '.' . $ext;
        // 说明：为了方便阅读，上一行代码进行的回车，如果你的遇到了问题，请放到一行

        // 构建鉴权对象
        $auth = new Auth(env('qiniu.ak', ''), env('qiniu.sk', ''));
        // 要上传的空间位置
        $token = $auth->uploadToken(env('qiniu.bucket', ''));

        // 初始化 UploadManager 对象并进行文件的上传。
        $uploadMgr = new UploadManager();
        list($ret, $err) = $uploadMgr->putFile($token, $newImageName, $filePath);
        if ($err !== null) {
            return null;
        } else {
            // 图片上传成功
            return env('qiniu.domain', '').'/'.$newImageName;
        }

    }
}
