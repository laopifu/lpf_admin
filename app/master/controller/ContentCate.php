<?php
/**
 * 内容分类
 */

namespace app\master\controller;

use app\common\Base;
use app\model\ContentCategoryModel;

class ContentCate extends Base {

    /**
     * 分类列表数据
     */
    public function cate_list(): \think\response\Json {
        $list = ContentCategoryModel::getTreeList();
        return success('分类数据', $list);
    }
}
