<?php
declare (strict_types=1);

namespace app;

use think\App;
use think\exception\ValidateException;
use think\Response;
use think\Validate;

//
// 下面2行，为了使用旧版的 success error redirect 跳转
//
use think\exception\HttpResponseException;
use think\facade\Request;

/**
 * 控制器基础类
 */
abstract class BaseController {
    /**
     * Request实例
     *
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     *
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     *
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     *
     * @var array
     */
    protected $middleware = [];

    /**
     * 构造方法
     *
     * @access public
     *
     * @param App $app 应用对象
     */
    public function __construct(App $app) {
        $this->app = $app;
        $this->request = $this->app->request;

        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize() {
    }

    /**
     * 验证数据
     *
     * @access protected
     *
     * @param array        $data     数据
     * @param string|array $validate 验证器名或者验证规则数组
     * @param array        $message  提示信息
     * @param bool         $batch    是否批量验证
     *
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false) {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }

    protected function verifyData(array $params, bool $batch = false) {
        $input = input();
        $data = [];
        $validate = [];
        foreach ($params as $value) {
            $param = $value[0];
            $arr = explode('|', $param);
            $value[0] = $arr[0];
            //如果传参中有设置的参数
            if (isset($input[$value[0]])) {
                $data[$value[0]] = $input[$value[0]];
            }
            if (!isset($input[$value[0]])) {
                if (isset($value[2])) {
                    $data[$value[0]] = $value[2];
                }
            }
            if (isset($value[1]) && $value[1] != '') {
                $validate[$param] = $value[1];
            }
        }
        $v = new Validate();
        $v->rule($validate);
        if ($batch) {
            $v->batch(true);
        }
        if (!$v->check($data)) {
            return [0, [], $v->getError()];
        } else {
            return [1, $data, 'ok'];
        }
    }
}
